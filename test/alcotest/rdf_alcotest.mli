(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** Alcotest testables for Rdf types. *)

val iri : Rdf.Iri.t Alcotest.testable
val blank_node : Rdf.Blank_node.t Alcotest.testable
val literal : Rdf.Literal.t Alcotest.testable
val subject : Rdf.Triple.Subject.t Alcotest.testable
val predicate : Rdf.Triple.Predicate.t Alcotest.testable
val object' : Rdf.Triple.Object.t Alcotest.testable
val term : Rdf.Term.t Alcotest.testable
val triple : Rdf.Triple.t Alcotest.testable
val graph : Rdf.Graph.t Alcotest.testable

<!--
SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>

SPDX-License-Identifier: CC0-1.0
-->

# ocaml-rdf

A RDF library for OCaml

# Content

`ocaml-rdf` consists of multiple sub-libraries that can be used individually:

- [`rdf`](./lib/core): Core RDF types and functions.

## Serialization

- [`rdf_json`](./lib/json): [RDF/JSON](https://www.w3.org/TR/rdf-json/). The parser and serializer are complete and should be compliant. However there are no official test cases published.
- [`rdf_xml`](./lib/xml): [RDF/XML](https://www.w3.org/TR/rdf-syntax-grammar/). The parser does not support XML canonicalisation. Apart from that the parser and serializer should be compliant and pass the official test cases.
- [`rdf_turtle`](./lib/turtle): [Turtle](https://www.w3.org/TR/turtle/). The parser is complete, compliant and passes the official test suites. Serializer is provided by Rdf_ntriples (a better Turtle serializer is not yet available).
- [`rdf_ntriples`](./lib/ntriples): [N-Triples](https://www.w3.org/TR/n-triples/). The parser is complete, compliant and passes the official test suites.
- [`rdf_cbor`](./lib/cbor): [RDF/CBOR](https://openengiadina.codeberg.page/rdf-cbor/). A binary, CBOR-based serialization of RDF. Support content-addressing of RDF.

## Testing

- [`rdf_alcotest`](./test/alcotest): Implements `Alcotest.testable` for the core RDF types. Useful when testing applications that use these types.
- [`rdf_gen`](./test/gen): Generators for [QCheck](https://github.com/c-cube/qcheck/). This is used within `ocaml-rdf` for property-based testing. It may also be useful for applications/libraries that use `ocaml-rdf`.
- [`rdf_graph_isomorphism_z3`](./lib/graph_isomorphism_z3): Uses the [Z3 Theorem Prover](https://github.com/Z3Prover/z3) to check if two graphs are isomorphic. Checking isomorpism of graphs that contain Blank Nodes is an NP-complete problem. This is only used for the serialization test cases. For real applications: don't use Blank Nodes.

# Contributing

Contributions are welcome as pull requests to the repository hosted at [codeberg](https://codeberg.org/openEngiadina/ocaml-rdf) or via email to pukkamustard [at] posteo [dot] net.

Copyright for submitted changes remain with the author and must be published under the same license as the project source code (AGPL-3.0-or-later).

Please make sure OCaml code is formatted with [ocamlformat](https://github.com/ocaml-ppx/ocamlformat) and appropriate SPDX copyright headers have been added (use the [reuse tool](https://reuse.software/)).

## Development

You may create a suitable environment for working on ocaml-rdf with [Guix](https://guix.gnu.org/):

``` sh
guix shell -D -f guix.scm
```

This will create an environment where you can run `dune build` to compile the library.

Currently it is hard to get a working development environment without Guix as some OCaml dependencies are not published to OPAM yet ([ocaml-cbor](https://inqlab.net/git/ocaml-cbor.git)).

# Related Software

## [OCaml-RDF by zoggy](https://www.good-eris.net/ocaml-rdf/)

There is another RDF library for OCaml developed by zoggy: [OCaml-RDF](https://www.good-eris.net/ocaml-rdf/) (in the following referred to as zoggy/ocaml-rdf). zoggy/ocaml-rdf is older and more mature than this RDF library and provides functionalities that are not included in this library (e.g. executing SPARQL queries). We were aware of zoggy/ocaml-rdf at the time we started implementing this library. We decided to create this alternate implementations for following reasons:

- Platform support: We needed an RDF library that works with [MirageOS](https://mirage.io/) and [js_of_ocaml](https://ocsigen.org/js_of_ocaml/). zoggy/ocaml-rdf is currently limited to Unix platforms as it uses the [Crytpokit](https://github.com/xavierleroy/cryptokit) library (which includes C code). 
- Environment for experimentation: This library was also developed to facilitate experimentation for research towards content-addressable RDF (see the `rdf_fragment_graph` module) and novel serializations (see `rdf_cbor`). We believe that following design decisions, that distiguish this library from zoggy/ocaml-rdf, made such experimentation easier:
	- Functional API: This library uses the standard library `Map` for a functional/persistent graph data structure, whereas zoggy/ocaml-rdf uses a mutable graph. We also use a neat little trick with [phantom types](https://wiki.haskell.org/Phantom_type) for making the `Rdf.Triple` API type-safer.
	- Multiple packages: In this library functionality is split into multiple sub-packages whereas zoggy/ocaml-rdf everyting is provided in a single package.

# Acknowledgments

ocaml-rdf was initially developed for the [DREAM](https://dream.public.cat/) project and has been supported trough the [NGI Pointer](https://pointer.ngi.eu/) fund.

Further work has been done for the [openEngiadina](https://openengiadina.net) project and has been supported by the [NLnet Foundation](https://nlnet.nl/) trough the [NGI0 Discovery Fund](https://nlnet.nl/discovery/).

# License

[AGPL-3.0-or-later](./LICENSES/AGPL-3.0-or-later.txt)

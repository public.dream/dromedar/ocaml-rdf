(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** RDF *)

module Iri = Uri

module Blank_node : sig
  type t
  (** A blank node. Don't use this. *)

  val of_string : string -> t
  (** Create a blank node using a string as identifier. *)

  val identifier : t -> string
  (** [identifier blank_node] returns the identifier from a BlankNode. *)

  val equal : t -> t -> bool
  (** [equal a b] returns true if [a] and [b] identify the same blank node. *)

  val compare : t -> t -> int
  (** [compare a b] compares two blank nodes by using the lexicographical sorting
      of the string identifiers. *)

  val generate : ?seed:Random.State.t -> unit -> t
  (** [generate ()] returns a blank node with a randomly generatated
      identifier. Identifiers are UUIDs (v4).

      If no random seed is given the current state is used (`Random.get_state ()`).*)

  val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  (**  [pp ppf t] will output a human readable version of the Blank_node [t] to the formatter [ppf] *)
end

module Literal : sig
  type t
  (** A literal. *)

  val make : string -> Iri.t -> t
  (** [make canonical datatype] returns a new literal with canonical
  representation [value] and [datatype]. *)

  val make_string : ?language:string -> string -> t
  (** [make_string value ~language] creates a new string literal. If
  [language] is provided the datatype rdf:langString is used and
  [language] is set as the language. If no [language] is provided the
  datatype xsd:string is used.  *)

  val make_boolean : bool -> t
  (** [make_boolean value] returns a new boolean literal. *)

  val make_float : float -> t
  (** [make_float value] returns a new float literal. Warning: This
  will cast [value] to a string.*)

  val make_integer : int -> t
  (** [make_integer value] returns a new integer literal. *)

  val canonical : t -> string
  (** [canonical literal] returns the canonical representation of the literal. *)

  val datatype : t -> Iri.t
  (** [datatype literal] returns the datatype of the literal. *)

  val language : t -> string option
  (** [language literal] returns the language tag of the literal. *)

  val equal : t -> t -> bool
  (** [equal a b] returns true if [a] and [b] are the same literal. *)

  val compare : t -> t -> int
  (** [compare a b] compares two literals by using the lexicographical sorting
      of the canonical values. *)

  val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  (**  [pp ppf t] will output a human readable version of the Literal [t] to the formatter [ppf] *)
end

module Term : sig
  (** A RDF term is either an IRI, a blank node or a literal. *)

  type t
  (** A RDF term *)

  val of_iri : Iri.t -> t
  (** [of_iri iri] casts the IRI [iri] to a term. *)

  val to_iri : t -> Iri.t option
  (** [to_iri term] attempts to casts [term] to an IRI. *)

  val of_blank_node : Blank_node.t -> t
  (** [of_blank_node bnode] casts the Blank Node [bnode] to a term. *)

  val to_blank_node : t -> Blank_node.t option
  (** [to_blank_node term] attempts to casts [term] to an Blank Node. *)

  val of_literal : Literal.t -> t
  (** [of_literal literal] casts the literal [literal] to a term. *)

  val to_literal : t -> Literal.t option
  (** [to_literal] attempts to casts [term] to a literal. *)

  val map :
    (Iri.t -> 'a) -> (Blank_node.t -> 'a) -> (Literal.t -> 'a) -> t -> 'a
  (** Map a term *)

  val equal : t -> t -> bool
  (** [equal a b] returns true if [a] and [b] are the equal. *)

  val compare : t -> t -> int
  (** [compare a b] defines a total order on terms. Returns 0 if [a] and [b] are
      equal, a negative integer if [a] is less than [b] and a positive integer if
      [a] is greater than [b].

      For RDF terms, IRI's are greater than Literals and Blank nodes and Literals are greater than Blank nodes.
  *)

  val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  (**  [pp ppf t] will output a human readable version of the Term [t] to the formatter [ppf] *)
end

module Triple : sig
  (** A triple is the basic building block of RDF data.

   A triple consists of three components:

   - A subject which is either an IRI or a blank node
   - A predicate which is an IRI
   - An object which is a literal, an IRI or a blank node

   *)

  module Subject : sig
    type t
    (** The subject of a triple. A subject may be either an IRI or a Blank Node.*)

    val of_iri : Iri.t -> t
    (** Create a subject from an Iri. *)

    val of_blank_node : Blank_node.t -> t
    (** Create a subject from a Blank Node*)

    val map : (Iri.t -> 'a) -> (Blank_node.t -> 'a) -> t -> 'a
    (** [map f_iri f_bnode s] maps the subject [s] by applying [f_iri] or
        [f_bnode] depending on wheter subject is an IRI or blank node.*)

    val to_term : t -> Term.t
    (** [to_term s] casts the subject to a Term. *)

    val to_iri : t -> Iri.t option
    (** [to_iri s] attempts to cast the subject to an IRI. *)

    val to_blank_node : t -> Blank_node.t option
    (** [to_blank_node s] attempts to cast the subject to a blank node. *)

    val equal : t -> t -> bool
    (** [equal a b] returns true if [a] and [b] are the equal. *)

    val compare : t -> t -> int
    (** [compare a b] defines a total order on subjects. Returns 0 if [a] and [b] are
      equal, a negative integer if [a] is less than [b] and a positive integer if
      [a] is greater than [b].

      For triple subjects, IRI's are greater than Blank nodes.
     *)

    val pp : t Fmt.t
      [@@ocaml.toplevel_printer]
    (**  [pp ppf t] will output a human readable version of the Subject [t] to the formatter [ppf] *)
  end

  module Predicate : sig
    type t
    (** The predicate of a triple which must be an Iri. *)

    val of_iri : Iri.t -> t
    (** Create an predicate from an Iri.*)

    val map : (Iri.t -> 'a) -> t -> 'a
    (** [map_predicate f p] applies [f] to the predicate IRI. *)

    val to_term : t -> Term.t
    (** [to_term p] casts the predicate to a Term. *)

    val to_iri : t -> Iri.t
    (** [to_iri p] casts the predicate to an IRI. *)

    val equal : t -> t -> bool
    (** [equal a b] returns true if [a] and [b] are the equal. *)

    val compare : t -> t -> int
    (** [compare a b] defines a total order on subjects. Returns 0 if [a] and [b] are
      equal, a negative integer if [a] is less than [b] and a positive integer if
      [a] is greater than [b].

      For triple predicates this is same as {!Rdf.Iri.compare}.
     *)

    val pp : t Fmt.t
      [@@ocaml.toplevel_printer]
    (**  [pp ppf t] will output a human readable version of the Predicate [t] to the formatter [ppf] *)
  end

  module Object : sig
    type t
    (** The object of a triple is either an IRI, a Blank Node or a literal.*)

    val of_iri : Iri.t -> t
    (** Create an object from an Iri.*)

    val of_blank_node : Blank_node.t -> t
    (** Create an object from a Blank Node.*)

    val of_literal : Literal.t -> t
    (** Create an object from a literal*)

    val of_term : Term.t -> t
    (** Create an object from a term. *)

    val map :
      (Iri.t -> 'a) -> (Blank_node.t -> 'a) -> (Literal.t -> 'a) -> t -> 'a
    (** Map an object *)

    val to_term : t -> Term.t
    (** [to_term o] casts the object to a Term. *)

    val to_iri : t -> Iri.t option
    (** [to_iri o] attempts to cast the object to an IRI. *)

    val to_blank_node : t -> Blank_node.t option
    (** [to_blank_node o] attempts to cast the object to an blank node. *)

    val to_literal : t -> Literal.t option
    (** [to_literal o] attempts to cast the object to a literal. *)

    val equal : t -> t -> bool

    val compare : t -> t -> int
    (** [compare a b] defines a total order on terms. Returns 0 if [a] and [b] are
      equal, a negative integer if [a] is less than [b] and a positive integer if
      [a] is greater than [b].

      For triple objects, IRI's are greater than Literals and Blank nodes and Literals are greater than Blank nodes. See also {!Term.compare}.
     *)

    val pp : t Fmt.t
      [@@ocaml.toplevel_printer]
    (**  [pp ppf t] will output a human readable version of the Object [t] to the formatter [ppf] *)
  end

  type t = { subject : Subject.t; predicate : Predicate.t; object' : Object.t }
  (** A triple. *)

  val make : Subject.t -> Predicate.t -> Object.t -> t
  (** Create a triple from a subject, predicate and object.*)

  val equal : t -> t -> bool

  val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  (**  [pp ppf t] will output a human readable version of the Triple [t] to the formatter [ppf] *)
end

module Namespace : sig
  (** RDF namespaces *)

  val make_namespace : string -> string -> Iri.t
  (** [make_namespace base_iri] returns a function that can be used to create IRIs in a namespace. *)

  val rdf : string -> Iri.t
  (** Create an IRI in the RDF namespace (`http://www.w3.org/1999/02/22-rdf-syntax-ns#`).*)

  val rdfs : string -> Iri.t
  (** Create an IRI in the RDFS namespace (`http://www.w3.org/2000/01/rdf-schema#`). *)

  val owl : string -> Iri.t
  (** Create an IRI in the OWL namespace (`http://www.w3.org/2002/07/owl#`). *)

  val xsd : string -> Iri.t
  (** Create an IRI in the XSD namespace (`http://www.w3.org/2001/XMLSchema#`). *)
end

module Description : sig
  (** A description is a set of triples with the same subject. *)

  type t
  (** A description *)

  val equal : t -> t -> bool
  (** [equal a b] tests wheter graphs [a] and [b] are equal. Note that this does
      not check for graph isomorphism (i.e. relabeling of blank nodes).*)

  val subject : t -> Triple.Subject.t
  (** [subject description] returns the subject of [description]. *)

  val empty : Triple.Subject.t -> t
  (** [empty subject] returns an empty description of [subject]. *)

  val add : Triple.Predicate.t -> Triple.Object.t -> t -> t
  (** [add predicate object' description] returns a new description that contains the triple [subject description], [predicate], [object'] and all triples in [description]. *)

  val objects : Triple.Predicate.t -> t -> Triple.Object.t Seq.t
  (** [objects predicate description] returns all objects for statements with [predicate]. *)

  val functional_property : Triple.Predicate.t -> t -> Triple.Object.t option
  (** [functional_property predicate description] returns the value of a functional property [predicate] if exists.

If property is not functional an unspecified propert value will be returned.

@see <https://www.w3.org/TR/owl-ref/#FunctionalProperty-def> *)

  val functional_property_iri : Triple.Predicate.t -> t -> Iri.t option
  (** [functional_property predicate description] returns the value of a functional property [predicate] if exists and is an IRI.

If property is not functional an unspecified propert value will be returned.

@see <https://www.w3.org/TR/owl-ref/#FunctionalProperty-def> *)

  val functional_property_literal : Triple.Predicate.t -> t -> Literal.t option
  (** [functional_property predicate description] returns the value of a functional property [predicate] if exists and is a literal.

If property is not functional an unspecified propert value will be returned.

@see <https://www.w3.org/TR/owl-ref/#FunctionalProperty-def> *)

  val to_triples : t -> Triple.t Seq.t
  (** [to_triples description] returns the triples of the description as a sequence. *)

  val to_nested_seq : t -> (Triple.Predicate.t * Triple.Object.t Seq.t) Seq.t
  (** [to_nested_seq description] returns a nested sequence of all statements for the subject of the description. *)
end

module Graph : sig
  (** A simple (and rather inefficient) implementation of an RDF graph. *)

  type t
  (** An RDF graph *)

  val equal : t -> t -> bool
  (** [equal a b] tests wheter graphs [a] and [b] are equal. Note that this does
      not check for graph isomorphism (i.e. relabeling of blank nodes).*)

  val empty : t
  (** [empty] is the empty graph. *)

  val singleton : Triple.t -> t
  (** [singleton triple] returns a graph containg only [triple]. *)

  val union : t -> t -> t
  (** [union a b] returns the union of the graphs. *)

  val add : Triple.t -> t -> t
  (** [add triple graph] returns a new graph containing [triple] (and
  everything in [graph]).*)

  val add_seq : Triple.t Seq.t -> t -> t
  (** [add_seq triples graph] returns a new graph containing all triples in the
      sequence [triples]. *)

  val remove : Triple.t -> t -> t
  (** [remove_triple triple graph] removes [triple] from [graph]. *)

  val subjects : t -> Triple.Subject.t Seq.t
  (** [subjects graph] returns a sequence of subjects that appear in the graph. *)

  val description : Triple.Subject.t -> t -> Description.t
  (** [description subject graph] returns the description of [subject] in [graph]. *)

  val descriptions : t -> Description.t Seq.t
  (** [descriptions graph] returns a sequence of descriptions that are
  contained in [graph]. *)

  val objects :
    Triple.Subject.t -> Triple.Predicate.t -> t -> Triple.Object.t Seq.t
  (** [objects subject predicate graph] returns all objects that appear as statements with [subject] and [predicate] in [graph]. *)

  val functional_property :
    Triple.Subject.t -> Triple.Predicate.t -> t -> Triple.Object.t option
  (** [functional_property subject predicate description] returns the value of a functional property [predicate] for [subject] if exists.

If property is not functional an unspecified propert value will be returned.

@see <https://www.w3.org/TR/owl-ref/#FunctionalProperty-def> *)

  val collection : Triple.Subject.t -> Triple.Predicate.t -> t -> Term.t Seq.t
  (** [collection subject predicate graph] returns the RDF collection at [subject], [predicate] as a sequence of terms.

@raises Invalid_argument when a literal is encountered while following the collection

@see <https://www.w3.org/TR/rdf11-mt/#rdf-collections> *)

  val of_triples : Triple.t Seq.t -> t
  (** [of_triples seq] returns a new graph containing all triples in [seq]. *)

  val to_triples : t -> Triple.t Seq.t
  (** [to_triples graph] returns a sequence of triples in [graph].*)

  val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  (**  [pp ppf t] will output a human readable version of the Graph [t] to
       the formatter [ppf] *)

  val to_nested_seq :
    t ->
    (Triple.Subject.t * (Triple.Predicate.t * Triple.Object.t Seq.t) Seq.t)
    Seq.t
  (** [to_nested_seq graph] returns a sequence of nested sequences
      containing all triples of [graph]. This is useful for serializing a graph. *)
end

module Decoder : sig
  (** This module provides decoders for reading OCaml values from an
  RDF Graph.

  Decoders can be combined in order to parse complex types (similar to
  parser combinators).*)

  type 'a t

  (** {1 Combinators} *)

  val return : 'a -> 'a t
  (** [return v] creates a decoder that will always succeed and return [v]. *)

  val fail : string -> 'a t
  (** [fail msg] creates a decoder that will always fail with the message [msg]. *)

  val map : ('a -> 'b) -> 'a t -> 'b t
  (** [map f d] returns a decoder that runs the decoder [d] and
  applies [f] to the decoded value. *)

  val bind : ('a -> 'b t) -> 'a t -> 'b t
  (** [bind f d] returns a decoder that runs [d], passes the result to [f] and runs the resulting decoder. *)

  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
  (** [p >>= f] is [bind f p].*)

  val ( >>| ) : 'a t -> ('a -> 'b) -> 'b t
  (** [p >>| f] is [map f p] *)

  val ( <$> ) : ('a -> 'b) -> 'a t -> 'b t
  (** [f <$> p] is equivalent to [p >>| f] *)

  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
  (** [f <*> p] is equivalent to [f >>= fun f -> p >>| f]. *)

  val ( *> ) : _ t -> 'a t -> 'a t
  (** [p <* q] runs [p], then runs [q], discards its result, and
  returns the result of [p]. *)

  val ( <* ) : 'a t -> _ t -> 'a t
  (** [p <* q] runs [p], then runs [q], discards its result, and
  returns the result of [p]. *)

  val ( <?> ) : 'a t -> string -> 'a t
  (** [p <?> msg] returns a parser that runs [p] and returns the
  result of [p] if [p] succeeds. If [p] fails the parser fails with
  message [msg]. *)

  val ( <|> ) : 'a t -> 'a t -> 'a t
  (** [p <|> q] returns a parser that runs [p] and returns the result
  of [p] if [p] succeeds. If [p] fails [q] is run. *)

  val choice : ?failure_msg:string -> 'a t list -> 'a t
  (** [choice ?failure_msg ts] runs each parser in [ts] in order until
  one succeeds and returns that result. In the case that none of the
  parser succeeds, then the parser will fail with the message
  [failure_msg], if provided, or a much less informative message
  otherwise. *)

  val option : 'a t -> 'a option t
  (** [option p] returns a parser that runs [p] and returns the result
  of [p] as [Some _] or [None] if [p] fails. *)

  (* {1 Term decoders} *)

  val any_iri : Iri.t t
  (** [iri] decodes an IRI. *)

  val iri : Iri.t -> unit t
  (** [iri v] returns a decoder that passes if current node is exactly the iri [v]. *)

  val any_literal : Literal.t t
  (** [literal] decodes a Literal. *)

  val any_blank_node : Blank_node.t t
  (** [blank_node] decodes a Literal. *)

  val any_term : Term.t t
  (** [term] decods a RDF term. Always succeeds. *)

  (* {1 Graph combinators} *)

  val subject : Triple.Subject.t t
  (** [subject] decodes the current node as a subject. *)

  val objects : Triple.Predicate.t -> 'a t -> 'a Seq.t t
  (** [objects predicate decoder] runs the [decoder] on all objects
that are connected to the currente node with [predicate]. *)

  val functional_property : Triple.Predicate.t -> 'a t -> 'a t
  (** [functional_property predicate decoder] runs the decoder at the
  value of the functional property [predicate].

  See also {Graph.functional_property}. *)

  (* {1 RDF Combinators} *)

  val type' : Iri.t -> unit t
  (** [type' iri] returns a decoder that suceeds if current node has
  rdf:type [iri]. *)

  (** {1 Decoding} *)

  val decode : Triple.Subject.t -> 'a t -> Graph.t -> ('a, string) Result.t
  (** [decode subject decoder graph] runs [decoder] on [graph]
  starting at node [subject]. *)
end

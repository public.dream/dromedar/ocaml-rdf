(*
 * SPDX-FileCopyrightText: 2021 petites singularités <ps-dream@lesoiseaux.io>
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type t = BNode of string

let of_string id = BNode id
let identifier (BNode id) = id
let equal a b = String.equal (identifier a) (identifier b)
let compare a b = String.compare (identifier a) (identifier b)

let generate ?(seed = Random.get_state ()) () =
  Uuidm.v4_gen seed () |> Uuidm.to_string |> of_string

let pp ppf bnode = Fmt.pf ppf "@[<h 1><blank_node@ %s>@]" (identifier bnode)

(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Term : sig
  module Iri : sig
    val to_cbor : Rdf.Iri.t -> Cborl.item
    val of_cbor : Cborl.item -> Rdf.Iri.t
  end

  module Literal : sig
    val to_cbor : Rdf.Literal.t -> Cborl.item
    val of_cbor : Cborl.item -> Rdf.Literal.t
  end

  module Blank_node : sig
    val to_cbor : Rdf.Blank_node.t -> Cborl.item
    val of_cbor : Cborl.item -> Rdf.Blank_node.t
  end

  (** {1 Serializaton} *)

  val encode : Rdf.Term.t -> Cborl.item
  val decode : Cborl.item -> Rdf.Term.t
  val read : Cborl.Signal.t Seq.t -> Rdf.Term.t * Cborl.Signal.t Seq.t
end

module Molecule : sig
  val encode : ?tag:bool -> Rdf.Graph.t -> Cborl.item
  val decode : Cborl.item -> Rdf.Graph.t
end

module Content_addressable : sig
  type t
  (** Type of a Content-addressable molecule - also known as a fragment molecule. *)

  (** {1 Predicate and Object} *)

  (** Content-addressable molecules can not reuse the
      {!module:Rdf.Triple.Predicate} and {!module:Rdf.Triple.Object}
      modules as they do not allow Blank Nodes and in addition to
      IRIs, references to fragments are allowed.*)

  module Predicate : sig
    type t
    (* Type of a predicate that can appear in a fragment molecule *)

    val base : t
    (** [base] use the base subject as predicate *)

    val of_iri : ?base_subject:Rdf.Iri.t -> Rdf.Iri.t -> t
    (** [of_iri iri] creates an Iri predicate  *)

    val make_fragment_reference : string -> t
    (** [make_fragment_reference fragment] creates a reference to [fragment] *)

    val expand : base_subject:Rdf.Iri.t -> t -> Rdf.Triple.Predicate.t
    (** [expand ~base_subject predicate] returns a predicate that can be used to
        construct a triple from [predicate]. Fragment references are expanded to
        full IRIs using the [base_subject].*)

    val pp : t Fmt.t [@@ocaml.toplevel_printer]
  end

  module Object : sig
    type t
    (** Type of object that can appear in a fragment molecule *)

    val base : t
    (** [base] use the base subject as object *)

    val of_iri : ?base_subject:Rdf.Iri.t -> Rdf.Iri.t -> t
    (** [of_iri iri] creates an iri object *)

    val of_literal : Rdf.Literal.t -> t
    (** [of_iri iri] creates an literal object *)

    val make_fragment_reference : string -> t
    (** [make_fragment_reference fragment] creates a reference to [fragment] *)

    val expand : base_subject:Rdf.Iri.t -> t -> Rdf.Triple.Object.t
    (** [expand ~base_subject predicate] returns an object that can be used to
        construct a triple from [object]. Fragment references are expanded to
        full IRIs using the [base_subject].*)

    val pp : t Fmt.t [@@ocaml.toplevel_printer]
  end

  module Namespace : sig
    (** Namespace helpers  *)

    val a : Predicate.t
    (** rdf:type *)

    val value : Predicate.t
    (** rdf:value *)
  end

  (** {1 Constructors} *)

  val empty : t
  (** [empty] is the empty fragment graph *)

  val add_statement : Predicate.t -> Object.t -> t -> t
  (** [add_statement predicate object' fragment_graph] adds a statement to the fragment graph. *)

  val add_opt_statement : Predicate.t -> Object.t option -> t -> t
  (** [add_opt_statement predicate object' fragment] adds a statement
  to the fragment graph if [object'] holds an object. Otherwise
  returns [fragment] unmodified. *)

  val add_fragment_statement : string -> Predicate.t -> Object.t -> t -> t
  (** [add_fragment_statement] adds a fragment statement to the fragment *)

  val add_opt_fragment_statement :
    string -> Predicate.t -> Object.t option -> t -> t
  (** [add_opt_statement predicate object' fragment] adds a fragment
  statement to the fragment if [object'] holds an object. Otherwise
  returns [fragment] unmodified. *)

  val of_triples : Rdf.Triple.t Seq.t -> (Rdf.Triple.Subject.t * t) Seq.t
  (** [of_triples triples] returns a sequence of content-addressable
      molecules for each base subject appearing in [triples].

     Note that [of_triples] eagerly emits fragments whenever a new
     base is found. If triples are not ordered, triples with same
     subject in [triples] may end up in different fragments. *)

  (* {1 Serialization } *)

  val encode : t -> Cborl.item
  val decode : Cborl.item -> t

  (* {1 Content-addressing } *)

  type hash = string -> Rdf.Iri.t
  (** Type of a hash function that returns a IRI. *)

  val base_subject : hash:hash -> t -> Rdf.Iri.t
  (** [base_subect ~hash fragment] returns the base subject of the
  fragment molecule [fragment] using the hash function [hash]. *)

  val to_triples : hash:hash -> t -> Rdf.Triple.t Seq.t
  (** [to_triples ~hash fragment] returns a sequence of triples in the
  fragment molecule [fragment. *)

  (** {1 (Pretty) Printing}  *)

  val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  (** [pp ppf t] will output a debug output of the Fragment Graph [t] to the formatter [ppf] *)
end

module Stream : sig
  type element =
    | Molecule of Rdf.Graph.t
    | Content_addressable of Content_addressable.t

  val write : element Seq.t -> Cborl.Signal.t Seq.t

  val read :
    hash:Content_addressable.hash -> Cborl.Signal.t Seq.t -> Rdf.Triple.t Seq.t
end

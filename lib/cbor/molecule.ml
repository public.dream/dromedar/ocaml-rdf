(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(* This module implements encoding of RDF/CBOR molecules *)

module TermSet = Set.Make (Rdf.Term)
module TermMap = Map.Make (Rdf.Term)

module Dictionary = struct
  type t = { terms : Rdf.Term.t array; lookup_map : int TermMap.t }

  let empty = { terms = [||]; lookup_map = TermMap.empty }

  (* Construct lookup map for terms *)
  let lookup_map terms =
    let _, lookup_map =
      Array.fold_left
        (fun (i, lookup) term -> (i + 1, TermMap.add term i lookup))
        (0, TermMap.empty) terms
    in
    lookup_map

  let of_graph graph =
    let subjects, pos =
      Rdf.Graph.to_triples graph
      |> Seq.fold_left
           (fun (subjects, pos) (triple : Rdf.Triple.t) ->
             ( subjects
               |> TermSet.add (Rdf.Triple.Subject.to_term triple.subject),
               pos
               |> TermSet.add (Rdf.Triple.Predicate.to_term triple.predicate)
               |> TermSet.add (Rdf.Triple.Object.to_term triple.object') ))
           (TermSet.empty, TermSet.empty)
    in
    let terms =
      [ subjects; TermSet.diff pos subjects ]
      |> List.to_seq
      |> Seq.concat_map TermSet.to_seq
      |> Array.of_seq
    in
    { terms; lookup_map = lookup_map terms }

  (* Locate and Extract *)

  let locate { lookup_map; _ } term = TermMap.find term lookup_map
  let locate_opt { lookup_map; _ } term = TermMap.find_opt term lookup_map
  let extract { terms; _ } id = Array.get terms id

  (* CBOR encoding *)

  (* helper to bind on none options *)
  let option_none_bind f opt = match opt with Some v -> v | None -> f ()

  let to_cbor { terms; _ } =
    terms |> Array.to_seq
    |> Seq.scan
         (fun (_, ie_prev) term ->
           let item, ie_prev' =
             match Rdf.Term.to_iri term with
             | Some iri -> Incremental_encoding.encode_iri ie_prev iri
             | None -> (Term.encode term, None)
           in
           (Some item, ie_prev'))
         (None, None)
    |> Seq.map fst
    |> Seq.filter_map (fun x -> x)
    |> List.of_seq
    |> fun l -> Cborl.Array l

  let of_cbor item =
    let term_to_iri_string term =
      Rdf.Term.map
        (fun iri -> Option.some @@ Rdf.Iri.to_string iri)
        (fun _ -> None)
        (fun _ -> None)
        term
    in
    match item with
    | Cborl.Array cbor_terms ->
        let terms =
          cbor_terms |> List.to_seq
          |> Seq.scan
               (fun (_, ie) item ->
                 match item with
                 | Cborl.Array [ Integer prefix_length; TextString suffix ]
                   when Option.is_some ie ->
                     let iri =
                       Incremental_encoding.decode ie prefix_length suffix
                     in
                     (Some (Rdf.Term.of_iri iri), Some (Rdf.Iri.to_string iri))
                 | _ ->
                     let term = Term.decode item in
                     (Some term, term_to_iri_string term))
               (None, None)
          |> Seq.map fst
          |> Seq.filter_map (fun x -> x)
          |> Array.of_seq
        in
        let lookup_map = lookup_map terms in
        { terms; lookup_map }
    | _ -> failwith "invalid encoding of dictionary"

  (* let ref_to_cbor i =
   *   (\* Implements the reference scheme as used in Packed CBOR *\)
   *   if i <= 15 then Cborl.Simple i
   *   else if Int.logand 1 i = 0 then
   *     (\* even *\)
   *     Cborl.(Tag (Z.(~$6), Integer (Z.of_int (16 + (2 * i)))))
   *   else Cborl.(Tag (Z.(~$6), Integer (Z.of_int (16 - (2 * i) - 1)))) *)
end

module Bitmap = struct
  let encode seq =
    seq
    |> Seq.fold_left
         (fun (i, b, l) group_seq ->
           let group = List.of_seq group_seq in
           let i' = i + List.length group in
           let n = i' - 1 in
           let b' = Z.(b lor shift_left one n) in
           (i', b', l @ group))
         (0, Z.zero, [])
    |> fun (_, bitmap, items) -> (bitmap, items)

  let first_group_size bitmap =
    let init_count = Z.popcount bitmap in
    let rec loop i =
      let count = Z.(popcount @@ shift_right bitmap i) in
      if count < init_count then i else loop (i + 1)
    in
    if init_count = 0 then 0 else loop 0

  let rec decode bitmap items () =
    let group_size = first_group_size bitmap in
    if group_size = 0 then Seq.Nil
    else
      Seq.Cons
        ( Array.sub items 0 group_size |> Array.to_seq,
          decode
            (Z.shift_right bitmap group_size)
            (Array.sub items group_size (Array.length items - group_size)) )
end

module TermStreams = struct
  let of_nested_seq dictionary seq =
    seq
    |> Seq.map (fun (_subject, predicate_objects) ->
           predicate_objects
           |> Seq.map (fun (predicate, objects) ->
                  ( Dictionary.locate dictionary
                    @@ Rdf.Triple.Predicate.to_term predicate,
                    objects
                    |> Seq.map (fun o ->
                           Dictionary.locate dictionary
                           @@ Rdf.Triple.Object.to_term o) ))
           |> Seq.unzip)
    |> Seq.unzip
    |> fun (p, o) -> (p, o |> Seq.concat)

  let decode dictionary predicates objects =
    let make_triple (s, p, o) =
      Rdf.Triple.(
        make
          (Rdf.Term.map Subject.of_iri Subject.of_blank_node
             (fun _ -> failwith "literal in subject position")
             s)
          (Rdf.Term.map Predicate.of_iri
             (fun _ -> failwith "blank node in predicate position")
             (fun _ -> failwith "literal in predicate position")
             p)
          (Object.of_term o))
    in
    let subject_predicates =
      Seq.mapi
        (fun s_id ->
          let s = Dictionary.extract dictionary s_id in
          Seq.map (fun p_id ->
              let p = Dictionary.extract dictionary p_id in
              (s, p)))
        predicates
      |> Seq.concat
    in
    Seq.map2
      (fun (s, p) ->
        Seq.map (fun o_id ->
            let o = Dictionary.extract dictionary o_id in
            (s, p, o)))
      subject_predicates objects
    |> Seq.concat |> Seq.map make_triple
end

let molecule_tag = Z.(~$301)

let encode ?(tag = false) graph =
  let dictionary = Dictionary.of_graph graph in

  let predicate_stream, object_stream =
    graph |> Rdf.Graph.to_nested_seq |> TermStreams.of_nested_seq dictionary
  in

  let predicate_bitmap, predicates = Bitmap.encode predicate_stream in
  let object_bitmap, objects = Bitmap.encode object_stream in

  let molecule_cbor =
    Cborl.(
      Array
        [
          Dictionary.to_cbor dictionary;
          Integer predicate_bitmap;
          Array (predicates |> List.map (fun i -> Integer (Z.of_int i)));
          Integer object_bitmap;
          Array (objects |> List.map (fun i -> Integer (Z.of_int i)));
        ])
  in

  if tag then Cborl.(Tag (molecule_tag, molecule_cbor)) else molecule_cbor

let int_of_cbor cbor =
  match cbor with
  | Cborl.Integer z -> Z.to_int z
  | _ -> failwith "expecting integer"

let rec decode item =
  match item with
  | Cborl.Tag (tag, item) when Z.(equal molecule_tag tag) -> decode item
  | Cborl.(
      Array
        [
          dictionary_cbor;
          Integer predicate_bitmap;
          Array predicates_cbor;
          Integer object_bitmap;
          Array objects_cbor;
        ]) ->
      let dictionary = Dictionary.of_cbor dictionary_cbor in
      let predicate_stream =
        predicates_cbor |> List.map int_of_cbor |> Array.of_list
        |> Bitmap.decode predicate_bitmap
      in
      let object_stream =
        objects_cbor |> List.map int_of_cbor |> Array.of_list
        |> Bitmap.decode object_bitmap
      in

      let graph =
        TermStreams.decode dictionary predicate_stream object_stream
        |> Rdf.Graph.of_triples
      in

      graph
  | _ -> failwith "invalid molecule"

let rec read seq =
  match Seq.uncons seq with
  | Some (Cborl.Signal.Tag tag, seq) when Z.(equal molecule_tag tag) -> read seq
  | Some (Cborl.Signal.Array (Some 5), seq) ->
      let dict_cbor, seq = Cborl.Signal.to_item seq in
      let predicate_bitmap, seq = Cborl.Signal.to_item seq in
      let predicates, seq = Cborl.Signal.to_item seq in
      let object_bitmap, seq = Cborl.Signal.to_item seq in
      let objects, seq = Cborl.Signal.to_item seq in

      let graph =
        decode
          Cborl.(
            Array
              [
                dict_cbor; predicate_bitmap; predicates; object_bitmap; objects;
              ])
      in
      (graph, seq)
  | Some _ -> failwith "expecting RDF/CBOR tag (300)"
  | None -> failwith "unexpected end of input"
